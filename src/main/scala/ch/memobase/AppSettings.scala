/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties

/** The kind of timestamp which should be used to compute sessions
  */
sealed trait TimeNotion

/** Timestamp of when the original event has been created.
  */
case object EventTime extends TimeNotion

/** Timestamp of when the report has been created.
  */
case object ReportTime extends TimeNotion

/** Timestamp of when the message has arrived at this service
  */
case object ProcessingTime extends TimeNotion

trait AppSettings {
  val kafkaStreamsProps: Properties = {
    val p = new Properties()
    p.setProperty("application.id", sys.env("APPLICATION_ID"))
    p.setProperty("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    p.setProperty("buffered.records.per.partition", "50")
    p
  }
  val elapsedTimeMultiplier: Float =
    sys.env.getOrElse("ELAPSED_TIME_MULTIPLIER", "1.0").toFloat
  if (elapsedTimeMultiplier < 0) {
    throw new Exception("ELAPSED_TIME_MULTIPLIER mustn't be negative")
  }
  val minimalInactivityGap: Long =
    sys.env.getOrElse("MINIMAL_INACTIVITY_GAP", "0").toLong
  if (minimalInactivityGap < 0) {
    throw new Exception("MINIMAL_INACTIVITY_GAP mustn't be negative")
  }
  val maximalInactivityGap: Option[Long] =
    sys.env.get("MAXIMAL_INACTIVITY_GAP").flatMap(x => Some(x.toLong))
  if (maximalInactivityGap.isDefined && maximalInactivityGap.get < minimalInactivityGap) {
    throw new Exception("MAXIMAL_INACTIVITY_GAP must be greater than MINIMAL_INACTIVITY_GAP")
  }
  val gracePeriod: Long =
    sys.env.getOrElse("GRACE_PERIOD", "3600000").toLong
  if (gracePeriod < 0) {
    throw new Exception("GRACE_PERIOD mustn't be negative")
  }
  val streamTimeNotion: TimeNotion =
    sys.env.getOrElse("STREAM_TIME", "processingtime").toLowerCase match {
      case "eventtime" => EventTime
      case "reporttime" => ReportTime
      case "processingtime" => ProcessingTime
      case other =>
        throw new Exception(
          s"$other is not a valid time notion. use `eventTime`, `processingTime` or `reportTime`"
        )
    }
  val punctuatorInterval: Long =
    sys.env.getOrElse("PUNCTUATOR_INTERVAL", "10000").toLong
  if (punctuatorInterval < 1) {
    throw new Exception("PUNCTUATOR_INTERVAL must be equal or greater than 1")
  }
  val topicIn: String = sys.env("TOPIC_IN")
  val steps: Array[String] = sys.env("STEPS").split(",").map(_.trim)
  val importApiDomain: String = sys.env("IMPORT_API_DOMAIN").stripSuffix("/")
  val importApiPort: Int = sys.env("IMPORT_API_PORT").toInt
  val incrementalUpdateSize: Option[Int] = sys.env.get("INCREMENTAL_UPDATE_CHUNK_SIZE").flatMap(size => Some(size.toInt))
  val shutdownGracePeriodMs = 10000
  val repartitionTopic: String = sys.env("REPARTITION_TOPIC")
}

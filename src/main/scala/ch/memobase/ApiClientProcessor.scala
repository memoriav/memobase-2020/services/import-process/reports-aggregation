/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.kafka.common.header.Headers
import org.apache.kafka.streams.processor.api.{
  Processor,
  ProcessorSupplier,
  Record
}
import org.apache.logging.log4j.scala.Logging
import sttp.client3._
import sttp.model.{MediaType, StatusCode, Uri}

import scala.util.{Failure, Success, Try}

class ApiClientProcessorSupplier
    extends ProcessorSupplier[String, String, String, String] {
  override def get(): Processor[String, String, String, String] =
    new ApiClientProcessor
}

object ApiClientProcessorSupplier {
  def apply(): ApiClientProcessorSupplier = new ApiClientProcessorSupplier()
}

class ApiClientProcessor
    extends Processor[String, String, String, String]
    with Logging
    with AppSettings {

  implicit class HeadersOps(headers: Headers) {

    def maybeValue(key: String): Try[String] = {
      //noinspection ScalaStyle
      headers.lastHeader(key) match {
        case null =>
          val msg = s"Header $key missing"
          logger.warn(msg)
          Failure(new NoSuchElementException(msg))
        case h => Success(new String(h.value))
      }
    }
  }

  var headers: Map[String, String] = _

  /** Wraps a request to clear the Drupal cache
    */
  private def clearCacheRequest(): Unit = {
    val uri = Uri(
      "http",
      importApiDomain,
      importApiPort,
      Seq("v1", "drupal", "clearcache")
    )
    sendClearCacheRequest(uri) match {
      case Failure(ex) =>
        logger.warn(s"Clear cache request to $uri failed: ${ex.getMessage}")
      case Success((code, msg)) if !code.isSuccess =>
        logger.warn(
          s"Clear cache request to $uri failed with status code $code: $msg"
        )
      case Success((code, _)) =>
        logger.debug(
          s"Clear cache request to $uri successful: $code"
        )
    }
  }

  /** Wraps a request to update the session status in Drupal
    * @param record: Record (i.e. message key and message value (metadata on the session status)
    */
  private def statusUpdateRequest(record: Record[String, String]): Unit = {
    val drupalJobUuid = record.headers.maybeValue("drupalJobUuid")
    val drupalJobLogResultUuid =
      record.headers.maybeValue("drupalJobLogResultUuid")
    if (drupalJobUuid.isSuccess && drupalJobLogResultUuid.isSuccess) {
      val uri = Uri(
        "http",
        importApiDomain,
        importApiPort,
        Seq("v1", "drupal", drupalJobUuid.get, drupalJobLogResultUuid.get)
      )
      sendStatusUpdateRequest(record.value, uri) match {
        case Failure(ex) =>
          logger.warn(
            s"Session status update request to $uri failed: ${ex.getMessage}"
          )
        case Success((code, msg)) if !code.isSuccess =>
          logger.warn(
            s"Session status update request to $uri failed with status code $code: $msg"
          )
        case Success((code, _)) =>
          logger.debug(
            s"Session status update request to $uri successful: $code"
          )
      }
    }
  }

  /** Sends a clear cache request to Import API
    * @param uri Respective import API endpoint
    * @return If successful, status code and status text; failure otherwise
    */
  private def sendClearCacheRequest(uri: Uri): Try[(StatusCode, String)] = Try {
    val backend = HttpURLConnectionBackend()
    val response = basicRequest
      .get(uri)
      .send(backend)
    (response.code, response.statusText)
  }

  /** Sends a status update request to Import API
    * @param aggregatedStatus Session status object
    * @param uri Respective import API endpoint
    * @return If successful, status code and status text; failure otherwise
    */
  private def sendStatusUpdateRequest(
      aggregatedStatus: String,
      uri: Uri
  ): Try[(StatusCode, String)] = Try {
    val backend = HttpURLConnectionBackend()
    val response = basicRequest
      .post(uri)
      .contentType(MediaType.ApplicationJson)
      .body(aggregatedStatus)
      .send(backend)
    (response.code, response.statusText)
  }

  override def process(record: Record[String, String]): Unit = {
    if (record.value == null) {
      clearCacheRequest()
    } else {
      statusUpdateRequest(record)
    }
  }

  override def close(): Unit = {}
}

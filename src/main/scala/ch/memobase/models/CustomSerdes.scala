/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}

import java.nio.charset.StandardCharsets

object CustomSerdes {
  private val separator = "###"

  implicit def aggregatedStatusSerde: Serde[AggregatedStatus] =
    new Serde[AggregatedStatus] {
      override def serializer: Serializer[AggregatedStatus] =
        new AggregatedStatusSerializer

      override def deserializer: Deserializer[AggregatedStatus] =
        new AggregatedStatusDeserializer
    }

  class AggregatedStatusDeserializer extends Deserializer[AggregatedStatus] {
    override def deserialize(
        topic: String,
        data: Array[Byte]
    ): AggregatedStatus = {
      val stats = genericDeserialize(data)
      AggregatedStatus(
        stats.head.toInt,
        stats(1).toInt,
        stats(2).toInt,
        stats(3).toInt,
        stats(4).toLong,
        stats(5).toLong,
        stats(6),
        stats(7),
        stats(8),
        stats(9),
        stats(10),
        stats(11),
        stats(12).toLong,
        stats(13).toLong,
        stats(14) == "1",
        stats(15),
        stats(16),
        stats(17).toInt
      )
    }
  }

  class AggregatedStatusSerializer extends Serializer[AggregatedStatus] {
    override def serialize(topic: String, data: AggregatedStatus): Array[Byte] =
      genericSerialize(
        data.success.toString,
        data.ignore.toString,
        data.warning.toString,
        data.fatal.toString,
        data.earliest.toString,
        data.latest.toString,
        data.step,
        data.sessionId,
        data.drupalJobUuid,
        data.drupalJobLogResultUuid,
        data.recordSetId,
        data.institutionId,
        data.streamTimestamp,
        data.maxElapsedTime,
        if (data.committed) "1" else "0",
        data.commitId,
        data.previousCommitId,
        data.version
      )
  }

  private def genericSerialize[T](elems: T*): Array[Byte] =
    elems.mkString(separator).getBytes(StandardCharsets.UTF_8)

  private def genericDeserialize(
      byteArray: Array[Byte]
  ): List[String] =
    new String(byteArray, StandardCharsets.UTF_8).split(separator).toList
}

/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.models

import ch.memobase.DateUtils.parseTimestamp
import org.apache.kafka.common.header.{Header, Headers}
import org.apache.logging.log4j.scala.Logging

import java.nio.charset.StandardCharsets
import java.util.UUID
import scala.util.{Failure, Success, Try}

/** A status count aggregation over reports belonging to a common session and step
  *
  * @param success Number of reports with status `SUCCESS`
  * @param ignore Number of reports with status `IGNORE`
  * @param warning Number of reports with status `WARNING`
  * @param fatal Number of reports with status `FATAL`
  * @param earliest Timestamp of earliest report in aggregation
  * @param latest Timestamp of latest report in aggregation
  * @param step Name of step to which the reports belong to
  * @param sessionId Session id to which the reports belong to
  * @param drupalJobUuid ID of the Drupal session the import job has been started
  * @param drupalJobLogResultUuid ID of the Drupal job log
  * @param recordSetId RecordSet id to which the reports belong to
  * @param institutionId Institution id to which the reports belong to
  * @param streamTimestamp Stream timestamp of the latest added report. This can, but doesn't have to be the same value as in `latest`
  * @param maxElapsedTime The maximal elapsed (stream) time between the addition of two reports
  * @param committed If the state of the aggregation is committed. It is regarded as such if the inactivityGap had
  *                  passed and no new records have been added since
  * @param version The version of the aggregation. It is increased by one whenever a new flushing happens
  * @param commitId The id of the commit (if object has been committed, otherwise empty)
  * @param previousCommitId The if of the previous commit (if any, otherwise empty)
  */
case class AggregatedStatus(
    success: Int = 0,
    ignore: Int = 0,
    warning: Int = 0,
    fatal: Int = 0,
    earliest: Long = 0,
    latest: Long = 0,
    step: String = "",
    sessionId: String = "",
    drupalJobUuid: String = "",
    drupalJobLogResultUuid: String = "",
    recordSetId: String = "",
    institutionId: String = "",
    streamTimestamp: Long,
    maxElapsedTime: Long = -1,
    committed: Boolean = false,
    commitId: String = "",
    previousCommitId: String = "",
    version: Int = 0
) extends Logging {

  import ch.memobase.DateUtils._

  /** Get a unique id of the aggregation. Consists of the session id and the step name
    * @return The unique id
    */
  def getId: String = s"$sessionId-$step"

  /** Generate a JSON object with the most important metrics
    * @return The stringified JSON object
    */
  def flush: String = {
    val earliestWithDefault =
      formatEpochTime(earliest).getOrElse("1970-01-01T00:00:00.000")
    val latestWithDefault =
      formatEpochTime(latest).getOrElse("1970-01-01T00:00:00.000")
    val elapsedTimeWithDefault =
      formatTimeDelta(latest - earliest).getOrElse("00:00:00.000")
    ujson
      .Obj(
        "sessionId" -> sessionId,
        "step" -> step,
        "total" -> total,
        "success" -> success,
        "ignore" -> ignore,
        "warning" -> warning,
        "fatal" -> fatal,
        "earliest" -> earliestWithDefault,
        "latest" -> latestWithDefault,
        "elapsedTime" -> elapsedTimeWithDefault,
        "recordSetId" -> recordSetId,
        "institutionId" -> institutionId,
        "messageId" -> commitId,
        "previousMessageId" -> previousCommitId,
        "messageVersion" -> version
      )
      .toString
  }

  /** Create a snapshot of the object
    * @return Snapshot object
    */
  def commit: AggregatedStatus = {
    val previousMessageId = this.commitId
    val messageId = UUID.randomUUID.toString
    this.copy(
      committed = true,
      version = version + 1,
      commitId = messageId,
      previousCommitId = previousMessageId
    )
  }

  /** Merge two `AggregatedStatus` objects
    * @param that The other object
    * @return The merged object
    */
  def merge(that: AggregatedStatus): AggregatedStatus = {
    logger.trace(
      s"Merging report for $sessionId/$step with timestamp ${that.latest}"
    )
    this.copy(
      success = success + that.success,
      ignore = ignore + that.ignore,
      warning = warning + that.warning,
      fatal = fatal + that.fatal,
      earliest = if (earliest < that.earliest && earliest > 0) {
        earliest
      } else {
        that.earliest
      },
      latest = preferIfGreater(that.latest, latest),
      step = preferIfNotEmpty(step, that.step),
      sessionId = preferIfNotEmpty(sessionId, that.sessionId),
      drupalJobUuid = preferIfNotEmpty(drupalJobUuid, that.drupalJobUuid),
      drupalJobLogResultUuid = preferIfNotEmpty(drupalJobLogResultUuid, that.drupalJobLogResultUuid),
      recordSetId = preferIfNotEmpty(recordSetId, that.recordSetId),
      institutionId = preferIfNotEmpty(institutionId, that.institutionId),
      streamTimestamp = preferIfGreater(that.streamTimestamp, streamTimestamp),
      maxElapsedTime = {
        val elapsedTime = that.streamTimestamp - streamTimestamp
        if (maxElapsedTime > elapsedTime) { maxElapsedTime }
        else { elapsedTime }
      },
      committed = false,
      commitId = commitId,
      previousCommitId = previousCommitId,
      version = if (version > that.version) { version }
      else { that.version }
    )
  }

  private def preferIfGreater(a: Long, b: Long): Long =
    if (a > b) a else b

  private def preferIfNotEmpty(s: String, fallback: String): String =
    if (s != "") s else fallback

  def hasReachedUpdateMark(chunkSize: Option[Int]): Boolean = chunkSize match {
    case Some(size) => total % size == 0
    case None       => false
  }

  private val total: Int = success + ignore + warning + fatal

}

object AggregatedStatus extends Logging {

  implicit class Stringify(h: Header) {
    def stringify: Try[String] =
      Option(h) match {
        case Some(s) => Success(new String(s.value, StandardCharsets.UTF_8))
        case None =>
          Failure(throw new Exception("No `sessionId` header present"))
      }
  }

  /** Creates an [[ch.memobase.models.AggregatedStatus]] instance.
    *
    * This is a representation of one message, which is further merged with
    * AggregatedStatuses for the same step in the same import job (i.e.
    * same session ids).
    *
    * @param value Message body (as JSON object)
    * @param headers Headers of message
    * @param timestamp Possible timestamp
    * @return Message representation as [[ch.memobase.models.AggregatedStatus]]
    */
  def apply(
      value: String,
      headers: Headers,
      timestamp: Option[Long]
  ): Try[AggregatedStatus] = Try {
    val json = ujson.read(value)
    val step = json("step").str

    val status = json("status").str.replace("IGNORED", "IGNORE")
    if (!List("SUCCESS", "IGNORE", "WARNING", "FATAL").contains(status)) {
      throw new Exception(s"$status is invalid status!")
    }

    val reportTimestamp = parseTimestamp(json("timestamp").str).get

    val streamTimestamp: Long = timestamp.getOrElse(reportTimestamp)

    val sessionId = headers.lastHeader("sessionId").stringify.get

    val drupalJobUuid = headers.lastHeader("drupalJobUuid").stringify.get

    val drupalJobLogResultUuid =
      headers.lastHeader("drupalJobLogResultUuid").stringify.get

    val recordSetId = headers
      .lastHeader("recordSetId")
      .stringify
      .get

    val institutionId = headers
      .lastHeader("institutionId")
      .stringify
      .get

    AggregatedStatus(
      success = if (status == "SUCCESS") 1 else 0,
      ignore = if (status == "IGNORE") 1 else 0,
      warning = if (status == "WARNING") 1 else 0,
      fatal = if (status == "FATAL") 1 else 0,
      earliest = reportTimestamp,
      latest = reportTimestamp,
      step = step,
      sessionId = sessionId,
      drupalJobUuid = drupalJobUuid,
      drupalJobLogResultUuid = drupalJobLogResultUuid,
      recordSetId = recordSetId,
      institutionId = institutionId,
      streamTimestamp = streamTimestamp
    )
  }
}

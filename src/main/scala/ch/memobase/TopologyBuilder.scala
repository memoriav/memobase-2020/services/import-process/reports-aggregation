/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.CustomSerdes.{
  AggregatedStatusDeserializer,
  AggregatedStatusSerializer
}
import org.apache.kafka.common.serialization.{
  StringDeserializer,
  StringSerializer
}
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.scala.Logging

//noinspection ScalaStyle
object TopologyBuilder extends AppSettings with Logging {

  import StatusAggregatorProcessorUtils._

  /**
    * Builds the overall topology of the stream application.
    *
    * The stream performs roughly the following steps:
    * 1. Reading in message from `topicIn` (as defined in [[ch.memobase.AppSettings]])
    * 2. Building an [[ch.memobase.models.AggregatedStatus]] instance from message,
    * using sessionId-stepId tuple as new message key
    * 3. Writing message in intermediary topic in order to allow aggregation per new key
    * 4. Branching to step-specific processors
    * 5. Merging [[ch.memobase.models.AggregatedStatus]] and sending the resulting object
    * downstream if time-based criteria are met (see [[ch.memobase.Session]])
    * 6. Sending aggregated object to external API
    * @return
    */
  def build: Topology = {
    val builder = new Topology
    builder
      .addSource(
        "source",
        new StringDeserializer,
        new StringDeserializer,
        topicIn
      )
      .addProcessor(
        "parser",
        ParserProcessorSupplier(streamTimeNotion),
        "source"
      )
      .addSink(
        "repartition-sink",
        repartitionTopic,
        new StringSerializer,
        new AggregatedStatusSerializer,
        "parser"
      )
      .addSource(
        "repartition-source",
        new StringDeserializer,
        new AggregatedStatusDeserializer,
        repartitionTopic
      )
      .addProcessor("branch", BranchProcessorSupplier(), "repartition-source")
      .addStatusAggregators(steps)
      .addProcessor(
        "final-sink",
        ApiClientProcessorSupplier(),
        steps.map(_ + "-aggregator"): _*
      )
  }
}

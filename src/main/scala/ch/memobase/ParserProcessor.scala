/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.AggregatedStatus
import org.apache.kafka.streams.processor.api.{
  Processor,
  ProcessorContext,
  ProcessorSupplier,
  Record
}
import org.apache.logging.log4j.scala.Logging

import java.time.Instant
import scala.util.{Failure, Success}

class ParserProcessorSupplier(timeNotion: TimeNotion)
    extends ProcessorSupplier[String, String, String, AggregatedStatus] {
  override def get(): Processor[String, String, String, AggregatedStatus] =
    new ParserProcessor(
      timeNotion
    )
}

object ParserProcessorSupplier {
  def apply(timeNotion: TimeNotion): ParserProcessorSupplier =
    new ParserProcessorSupplier(timeNotion)
}

class ParserProcessor(timeNotion: TimeNotion)
    extends Processor[String, String, String, AggregatedStatus]
    with Logging {

  var context: ProcessorContext[String, AggregatedStatus] = _

  override def init(context: ProcessorContext[String, AggregatedStatus]): Unit =
    this.context = context

  override def process(record: Record[String, String]): Unit =
    record.key match {
      case k: String if k.startsWith("https://memobase.ch/record/") => {
        AggregatedStatus(
          record.value,
          record.headers,
          timeNotion match {
            case EventTime      => Some(record.timestamp)
            case ReportTime     => None
            case ProcessingTime => Some(Instant.now.toEpochMilli)
          }
        ) match {
          case Success(aS) =>
            context.forward(
              record
                .withKey(aS.getId)
                .withTimestamp(aS.streamTimestamp)
                .withValue(aS)
            )
          case Failure(ex) =>
            logger.warn(
              s"Couldn't parse report ${record.key}: ${ex.getMessage}"
            )
            logger.debug(s"Report content: ${record.value}")
        }
        context.commit()
      }
      case k: String if k.startsWith("https://memobase.ch/recordSet/") =>
        logger.info(s"$k: Reports on record set imports are ignored")
      case k: String if k.startsWith("https://memobase.ch/institution/") =>
        logger.info(s"$k: Reports on institution imports are ignored")
      case k: String =>
        logger.warn(
          s"Report with unknown key $k! This report is skipped"
        )
      case k =>
        logger.warn(
          "Report with unknown key type! This report is skipped"
        )
    }

  override def close(): Unit = {}
}

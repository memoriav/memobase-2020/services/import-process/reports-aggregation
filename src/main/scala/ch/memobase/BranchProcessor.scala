/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.AggregatedStatus
import org.apache.kafka.streams.errors.StreamsException
import org.apache.kafka.streams.processor.api.{
  Processor,
  ProcessorContext,
  ProcessorSupplier,
  Record
}
import org.apache.logging.log4j.scala.Logging

class BranchProcessorSupplier
    extends ProcessorSupplier[
      String,
      AggregatedStatus,
      String,
      AggregatedStatus
    ] {
  override def get()
      : Processor[String, AggregatedStatus, String, AggregatedStatus] =
    new BranchProcessor
}

object BranchProcessorSupplier {
  def apply(): BranchProcessorSupplier = new BranchProcessorSupplier()
}

class BranchProcessor
    extends Processor[String, AggregatedStatus, String, AggregatedStatus]
    with Logging {
  var context: ProcessorContext[String, AggregatedStatus] = _

  override def init(
      context: ProcessorContext[String, AggregatedStatus]
  ): Unit = {
    this.context = context
  }

  override def process(record: Record[String, AggregatedStatus]): Unit = {
    try {
      context.forward(record, s"${record.value.step}-aggregator")
    } catch {
      case _: StreamsException =>
        logger.warn(
          s"Tried to reach processor ${record.value.step}-aggregator which does not exists. Maybe add step ${record.value.step} to `STEPS` envvar?"
        )

    }
  }

  override def close(): Unit = {}
}

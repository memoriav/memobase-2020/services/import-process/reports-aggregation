/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.DateUtils.formatEpochTime
import ch.memobase.models.AggregatedStatus
import ch.memobase.models.CustomSerdes.aggregatedStatusSerde
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.processor.api._
import org.apache.kafka.streams.processor.{Cancellable, PunctuationType}
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.streams.state.{
  Stores,
  TimestampedKeyValueStore,
  ValueAndTimestamp
}
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.jdk.CollectionConverters._

class StatusAggregationProcessorSupplier(stepName: String)
    extends ProcessorSupplier[
      String,
      AggregatedStatus,
      String,
      String
    ] {
  override def get(): Processor[String, AggregatedStatus, String, String] =
    new StatusAggregatorProcessor(stepName)
}

object StatusAggregationProcessorSupplier {
  def apply(stepName: String): StatusAggregationProcessorSupplier =
    new StatusAggregationProcessorSupplier(stepName)
}

class StatusAggregatorProcessor(stepName: String)
    extends Processor[String, AggregatedStatus, String, String]
    with Logging
    with AppSettings {
  private var context: ProcessorContext[String, String] = _
  private var store: TimestampedKeyValueStore[String, AggregatedStatus] = _
  private var cancellableWallClockTimePunctuator: Cancellable = _
  private val session =
    new Session(
      elapsedTimeMultiplier,
      minimalInactivityGap,
      maximalInactivityGap,
      gracePeriod
    )
  var latestTimestamp: Long = -1

  override def init(
      context: ProcessorContext[String, String]
  ): Unit = {
    this.context = context
    this.store = context
      .getStateStore(s"$stepName-store")
      .asInstanceOf[TimestampedKeyValueStore[String, AggregatedStatus]]
    this.cancellableWallClockTimePunctuator = context.schedule(
      Duration.ofMillis(punctuatorInterval),
      PunctuationType.WALL_CLOCK_TIME,
      timestamp =>
        flushStore(
          timestamp,
          PunctuationType.WALL_CLOCK_TIME,
          partitionNum = context.recordMetadata.get.partition
        )
    )
  }

  /** Merges [[ch.memobase.models.AggregatedStatus]] of the same step and sessionId
    * and sends actualised aggregation downstream if criteria for a release are met
    * (see [[ch.memobase.Session]]).
    *
    * @param record Kafka message
    */
  override def process(record: Record[String, AggregatedStatus]): Unit = {
    val key = record.key
    val value = record.value

    val streamTime = value.streamTimestamp
    logger.debug(
      s"Extracted timestamp for aggregation $key: ${formatEpochTime(streamTime).getOrElse("<unknown>")}"
    )
    if (latestTimestamp > streamTime) {
      logger.warn(
        s"""Stream time is going backwards! Previous timestamp ($latestTimestamp) > this timestamp ($streamTime).
           | This can lead to premature flushings of and therefore false aggregations!
           | You should probably change the `timeNotion` setting to something more reliable.""".stripMargin
      )
    }
    latestTimestamp = streamTime
    val aggregation = Option(store.get(key)) match {
      case Some(valueAndTimestamp) =>
        valueAndTimestamp.value.merge(value)
      case None =>
        value
    }
    store.put(
      key,
      ValueAndTimestamp
        .make[AggregatedStatus](aggregation, System.currentTimeMillis)
    )

    flushStore(
      streamTime,
      PunctuationType.STREAM_TIME,
      context.recordMetadata.get.partition
    )

  }

  override def close(): Unit = {
    cancellableWallClockTimePunctuator.cancel()
  }

  private def flushStore(
      now: Long,
      punctuationType: PunctuationType,
      partitionNum: Int
  ): Unit = {
    val storeIterator = store.all
    storeIterator.asScala
      .foreach(agg => {
        if (
          (session.inactive(
            agg,
            now,
            punctuationType,
            partitionNum
          ) || agg.value.value.hasReachedUpdateMark(
            incrementalUpdateSize
          )) && !agg.value.value.committed
        ) {
          val newValue = agg.value.value.commit
          val headers = new RecordHeaders()
            .add("drupalJobUuid", agg.value.value.drupalJobUuid.getBytes)
            .add(
              "drupalJobLogResultUuid",
              agg.value.value.drupalJobLogResultUuid.getBytes
            )
          val newRecord = new Record[String, String](
            agg.key,
            newValue.flush,
            agg.value.timestamp,
            headers
          )
          context.forward[String, String](newRecord)
          store.put(
            agg.key,
            ValueAndTimestamp
              .make[AggregatedStatus](newValue, agg.value.timestamp)
          )
        }
        if (session.expired(agg, now, punctuationType, partitionNum)) {
          store.delete(agg.key)
          //noinspection ScalaStyle
          val newRecord =
            new Record[String, String](agg.key, null, agg.value.timestamp)
          context.forward[String, String](newRecord)
        }
      })
    storeIterator.close()
  }
}

object StatusAggregatorProcessorUtils {

  implicit class StatusAggregatorAdder(topology: Topology) {
    def addStatusAggregators(stepNames: Array[String]): Topology =
      stepNames.foldLeft(topology)((topology, stepName) =>
        topology.addStatusAggregator(stepName)
      )

    def addStatusAggregator(stepName: String): Topology =
      topology
        .addProcessor(
          s"$stepName-aggregator",
          StatusAggregationProcessorSupplier(stepName),
          "branch"
        )
        .addStateStore(
          Stores
            .timestampedKeyValueStoreBuilder(
              Stores.persistentTimestampedKeyValueStore(s"$stepName-store"),
              Serdes.stringSerde,
              aggregatedStatusSerde
            )
            .withLoggingDisabled,
          s"$stepName-aggregator"
        )
  }

}

/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.memobase

import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.util.{Failure, Success, Try}

object App extends scala.App with AppSettings with Logging {

  logger.info(s"ELAPSED_TIME_MULTIPLIER set to $elapsedTimeMultiplier")
  logger.info(s"MINIMAL_INACTIVITY_GAP set to $minimalInactivityGap")
  if (maximalInactivityGap.isDefined) {
    logger.info(s"MAXIMAL_INACTIVITY_GAP set to ${maximalInactivityGap.get}")
  } else {
    logger.info(s"MAXIMAL_INACTIVITY_GAP not set")
  }
  logger.info(s"GRACE_PERIOD set to $gracePeriod")
  logger.info(s"STREAM_TIME set to $streamTimeNotion")
  logger.info(s"PUNCTUATOR_INTERVAL set to $punctuatorInterval")
  logger.info(s"TOPIC_IN set to $topicIn")
  logger.info(s"STEPS set to -> ${steps.mkString("|")}")
  logger.info(s"IMPORT_API_HOST set to $importApiDomain")

  val topology = TopologyBuilder.build
  val streams = new KafkaStreams(
    topology,
    kafkaStreamsProps
  )

  logger.debug(topology.describe().toString)

  logger.trace("Starting stream processing")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Kafka Streams workflow successfully built")
    case Failure(f) =>
      logger.error(
        s"Aborting Kafka Streams workflow build process due to errors: ${f.getMessage}"
      )
      sys.exit(1)
  }

  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }
}

/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.AggregatedStatus
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.processor.PunctuationType
import org.apache.kafka.streams.state.ValueAndTimestamp
import org.apache.logging.log4j.scala.Logging

/** Abstracts over a "session". A session in this context is defined as a group of reports
 * a) having the same session id and the same step and
 * b) sharing a common time period in which they were generated.
 *
 * A time period finishes when no new reports were generated in a defined amount of time. The later is computed dynamically
 * based on the highest difference between the stream timestamps of two records, multplied by a customisable factor (`elapsedTimeMultiplier`).
 * If the computed value is lower than the also customisable value of `minimalInactivityGap`, the later is used.
 *
 * Furthermore there is a `gracePeriod` in which out-of-time arrivals are accepted as being part of the same session. The sum
 * of `inactivityGap` and `gracePeriod` marks the period after which a session is considered as expired.
 *
 * @param elapsedTimeMultiplier A multiplier used to compute a dynamic inactivity gap based on the highest measured duration
 *                              in between the stream timestamps of two reports
 * @param minimalInactivityGap  Time in milliseconds after which a session is at the earliest considered inactive
 * @param maximalInactivityGap  Time in milliseconds after which a session is at the latest considered inactive
 * @param gracePeriod           Time in milliseconds after which an _inactive_ session is considered expired (i.e. a session is expired when
 *                              `<time of last activity> + <inactivityGap> + <gracePeriod> < <actual time>` holds)
 */
class Session(
               elapsedTimeMultiplier: Float,
               minimalInactivityGap: Long,
               maximalInactivityGap: Option[Long],
               gracePeriod: Long
             ) extends Logging {

  def inactive(
                agg: KeyValue[String, ValueAndTimestamp[AggregatedStatus]],
                now: Long,
                punctuationType: PunctuationType,
                partitionNum: Int
              ): Boolean = {
    val msgTimestamp = timestampExtractor(agg.value, punctuationType)
    val inactivityGap = computeInactivityGap(agg)
    val isInactive = (msgTimestamp + inactivityGap) < now
    if (isInactive) {
      val partitionSnippet = if (partitionNum > -1) {
        s" in partition $partitionNum"
      } else {
        " "
      }
      logger.debug(
        s"Aggregation ${agg.key} based on ${timestampTypeName(punctuationType)}$partitionSnippet is inactive: ${
          DateUtils
            .formatEpochTime(msgTimestamp)
            .getOrElse("<unknown>")
        } + ${inactivityGap}ms < ${
          DateUtils
            .formatEpochTime(now)
            .getOrElse("<unknown>")
        }"
      )
    }
    isInactive
  }

  def expired(
      agg: KeyValue[String, ValueAndTimestamp[AggregatedStatus]],
      now: Long,
      punctuationType: PunctuationType,
      partitionNum: Int
  ): Boolean = {
    val msgTimestamp = timestampExtractor(agg.value, punctuationType)
    val inactivityGap =
      computeInactivityGap(agg)
    val isExpired = (msgTimestamp + inactivityGap + gracePeriod) < now
    if (isExpired) {
      val partitionSnippet = if (partitionNum > -1) {
        s" in partition $partitionNum"
      } else {
        " "
      }
      logger.debug(
        s"Aggregation ${agg.key} based on ${timestampTypeName(punctuationType)}$partitionSnippet has expired: ${
          DateUtils
            .formatEpochTime(msgTimestamp)
            .getOrElse("<unknown>")
        } + ${inactivityGap}ms + ${gracePeriod}ms < ${
          DateUtils
            .formatEpochTime(now)
            .getOrElse("<unknown>")
        }"
      )
    }
    isExpired
  }

  private def timestampExtractor(
                                  value: ValueAndTimestamp[AggregatedStatus],
                                  punctuationType: PunctuationType
                                ): Long = {
    punctuationType match {
      case PunctuationType.STREAM_TIME => value.value.streamTimestamp
      case PunctuationType.WALL_CLOCK_TIME => value.timestamp
    }
  }

  private def computeInactivityGap(
                                    agg: KeyValue[String, ValueAndTimestamp[AggregatedStatus]]
                                  ): Long = {
    val dynamicInactivityGap =
      math.round(agg.value.value.maxElapsedTime * elapsedTimeMultiplier)
    val inactivityGap = if (minimalInactivityGap > dynamicInactivityGap) {
      minimalInactivityGap
    } else if (maximalInactivityGap.exists(_ < dynamicInactivityGap)) {
      maximalInactivityGap.get
    } else {
      dynamicInactivityGap
    }
    logger.debug(s"Set dynamic inactivity gap for ${agg.key} to $inactivityGap")
    inactivityGap
  }

  private def timestampTypeName(punctuationType: PunctuationType) =
    punctuationType match {
      case PunctuationType.STREAM_TIME => "stream time"
      case PunctuationType.WALL_CLOCK_TIME => "wall clock time"
    }

}

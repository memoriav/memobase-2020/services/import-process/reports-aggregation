/*
 * Reports Aggregation
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.text.SimpleDateFormat
import java.util.Date
import scala.util.Try

object DateUtils {
  private val dateFormatWithMillis = new SimpleDateFormat(
    "yyyy-MM-dd'T'HH:mm:ss.SSS"
  )
  private val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

  def parseTimestamp(timestamp: String): Try[Long] = Try {
    dateFormatWithMillis.parse(timestamp).getTime
  }.orElse(Try(dateFormat.parse(timestamp).getTime))

  def formatEpochTime(epochTime: Long): Try[String] = Try {
    val date = new Date(epochTime)
    dateFormatWithMillis.format(date)
  }

  def formatTimeDelta(timeDelta: Long): Try[String] = Try {
    val ms = timeDelta % 1000
    val sec = (timeDelta / 1000) % 60
    val min = (timeDelta / (60 * 1000)) % 60
    val hrs = timeDelta / (60 * 60 * 1000)
    f"$hrs%02d:$min%02d:$sec%02d.$ms%03d"
  }
}

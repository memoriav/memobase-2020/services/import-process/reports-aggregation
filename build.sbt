import Dependencies._

ThisBuild / scalaVersion := "2.13.14"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Reports Aggregation",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml"       => MergeStrategy.first
      case other if other.contains("scala/annotation/nowarn.class") =>
        MergeStrategy.first
      case other if other.contains("scala/annotation/nowarn$.class") =>
        MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.memobase.App"),
    libraryDependencies ++= Seq(
      kafkaStreams,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      scalaUri,
      sttp,
      upickle,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test
    )
  )
